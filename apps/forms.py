# coding=utf-8
from django import forms
from django.conf import settings


class AddEventForm(forms.Form):
    summary = forms.CharField(label=u'Заголовок', max_length=200, widget=forms.TextInput(attrs={'class': 'form-control'}))
    description = forms.CharField(label=u'Описание', max_length=200, widget=forms.TextInput(attrs={'class': 'form-control'}))
    start = forms.CharField(label=u'Начало', widget=forms.TextInput(attrs={'class': 'form-control calendar-field'}))
    end = forms.CharField(label=u'Окончание', widget=forms.TextInput(attrs={'class': 'form-control calendar-field'}))
    attende = forms.EmailField(label=u'Участник', widget=forms.EmailInput(attrs={'class': 'form-control'}))
    # datemime = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'class': 'form-control'}))

class AddSiteForm(forms.Form):
    name = forms.CharField(label=u'Заголовок', max_length=200, widget=forms.TextInput(attrs={'class': 'form-control'}))
    url = forms.URLField(label=u'Адрес сайта')