#coding=utf-8
from django.test import TestCase


from django.test.client import Client

class AppTestCase(TestCase):
    fixtures = ['test_data.json']
    def test_login(self):
        c = Client()
        response = c.post('/accounts/login/', {'username': 'root', 'password': '1234567890-'})
        #Если логин и пароль верный то переходим на 302 редирект на главную страницу
        self.assertEqual(response.status_code,302)

    def test_main_page(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def test_list_page(self):
        c = Client()
        c.post('/accounts/login/', {'username': 'root', 'password': '1234567890-'})
        response = c.get('/list/')
        self.assertEqual(response.status_code, 200)

    def test_add_page(self):
        c = Client()
        c.post('/accounts/login/', {'username': 'root', 'password': '1234567890-'})
        response = c.get('/add/')
        self.assertEqual(response.status_code, 200)

    def test_real_page(self):
        c = Client()
        c.post('/accounts/login/', {'username': 'root', 'password': '1234567890-'})
        response = c.get('/real/')
        self.assertEqual(response.status_code, 200)